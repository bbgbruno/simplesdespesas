﻿using System.Data.Entity;

namespace Modelos
{
    public class DataContext : DbContext
    {
        public DbSet<PlanoDeConta> PlanoDeContas { get; set; }
        public DbSet<TipoPagamento> TipoPagamentos { get; set; }
        public DbSet<Movimento> Movimentos { get; set; }
        public DbSet<Usuario> Usuarios { get; set; }  

    }
    
}