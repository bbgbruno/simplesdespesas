﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Modelos;

namespace Repositorio.Interfaces
{
    class MovimentoRepositorio : IMovimentoRepositorio
    {
        
        private DataContext _context;

        public MovimentoRepositorio()
        {
            _context = new DataContext();
        }

        public MovimentoRepositorio(DataContext context)
        {
            _context = context;
        }

                
        public List<Modelos.Movimento> List(int idUsuario)
        {
            throw new NotImplementedException();
        }

        public Modelos.Movimento Detalhes(int id, int idUsuario)
        {
            throw new NotImplementedException();
        }

        public void Incluir(Modelos.Movimento movimento, Modelos.Usuario usuario)
        {
            throw new NotImplementedException();
        }

        public void alterar(int id, Modelos.Movimento novoMovimento, int idUsuario)
        {
            throw new NotImplementedException();
        }

        public void Excluir(int Id, int idUsuario)
        {
            throw new NotImplementedException();
        }
    }
}
