﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Modelos;

namespace Repositorio.Interfaces
{
    class TipoPagamentoRepositorio : ITipoPagamentoRepositorio
    {
       private DataContext _context;

        public TipoPagamentoRepositorio()
        {
            _context = new DataContext();
        }

        public TipoPagamentoRepositorio(DataContext context)
        {
            _context = context;
        }

        public List<TipoPagamento> List(int idUsuario)
        {
            return _context.TipoPagamentos.Where(p => p.Usuario.Id == idUsuario).ToList();
        }

        public TipoPagamento Detalhes(int id,int idUsuario)
        {
            return _context.TipoPagamentos.Where(p => p.Id == id && p.Usuario.Id == idUsuario).FirstOrDefault();
        }

        public void Incluir(TipoPagamento tipoPagamento, Usuario usuario)
        {
            tipoPagamento.Usuario = _context.Usuarios.Find(usuario.Id);
            _context.TipoPagamentos.Add(tipoPagamento);
            _context.SaveChanges();
        }

        public void alterar(int id, TipoPagamento novoPlanoDeConta, int idUsuario)
        {
            TipoPagamento tipoPagamento = _context.TipoPagamentos.Where(p => p.Id == id && p.Usuario.Id == idUsuario).FirstOrDefault();
            tipoPagamento.limite = novoPlanoDeConta.limite;
            tipoPagamento.Descricao = novoPlanoDeConta.Descricao;
            _context.SaveChanges();
        }

        public void Excluir(int id, int idUsuario)
        {
            _context.PlanoDeContas.Remove(_context.PlanoDeContas.Where(p => p.Id == id && p.Usuario.Id == idUsuario).FirstOrDefault());
            _context.SaveChanges();
        }
    }
}
