﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Modelos;

namespace Repositorio.Interfaces
{
   public class UsuarioRepositorio : IUsuarioRepositorio
    {
        private DataContext _context;

        #region Construtores

        public UsuarioRepositorio()
        {
            _context = new DataContext();
        }

        public UsuarioRepositorio(DataContext context)
        {
            _context = context;
        }

        #endregion

        public Usuario Buscar(string membershipUserKey)
        {
            Usuario usuario = _context.Usuarios.Where(p => p.MembershipUserKey == membershipUserKey).Single();
            return usuario; 
        }



       public List<Usuario> List()
        {
            throw new NotImplementedException();
        }

        public Usuario Detalhes(int id)
        {
            throw new NotImplementedException();
        }

        public void Incluir(Usuario usuario)
        {
            _context.Usuarios.Add(usuario);
            _context.SaveChanges();
        }

        public void alterar(int id, Usuario novoUsuario)
        {
            throw new NotImplementedException();
        }

        public void Excluir(int Id)
        {
            throw new NotImplementedException();
        }
    }
}
