﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Modelos;

namespace Repositorio.Interfaces
{
    public class PlanoDeContasRepositorio : IPlanoDeContasRepositorio
    {
        private DataContext _context;

        public PlanoDeContasRepositorio()
        {
            _context = new DataContext();
        }

        public PlanoDeContasRepositorio(DataContext context)
        {
            _context = context;
        }

        public List<PlanoDeConta> List(int idUsuario)
        {
            return _context.PlanoDeContas.Where(p => p.Usuario.Id == idUsuario).ToList();
        }

        public PlanoDeConta Detalhes(int id,int idUsuario)
        {
            return _context.PlanoDeContas.Where(p => p.Id == id && p.Usuario.Id == idUsuario).FirstOrDefault();
        }

        public void Incluir(PlanoDeConta planodeconta, Usuario usuario)
        {
            planodeconta.Usuario = _context.Usuarios.Find(usuario.Id);
            _context.PlanoDeContas.Add(planodeconta);
            _context.SaveChanges();
        }

        public void alterar(int id, PlanoDeConta novoPlanoDeConta, int idUsuario)
        {
            PlanoDeConta planoDeconta = _context.PlanoDeContas.Where(p => p.Id == id && p.Usuario.Id == idUsuario).FirstOrDefault();
            planoDeconta.Meta = novoPlanoDeConta.Meta;
            planoDeconta.Descricao = novoPlanoDeConta.Descricao;
            planoDeconta.TipoPlanoDeConta = novoPlanoDeConta.TipoPlanoDeConta;
            _context.SaveChanges();
        }

        public void Excluir(int id, int idUsuario)
        {
            _context.PlanoDeContas.Remove(_context.PlanoDeContas.Where(p => p.Id == id && p.Usuario.Id == idUsuario).FirstOrDefault());
            _context.SaveChanges();
        }
    }
}
