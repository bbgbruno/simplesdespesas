﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Modelos;
using Repositorio.Interfaces;

namespace SimplesDespesas.Controllers
{ 
    public class PlanoDeContaController : AplicationController
    {

        PlanoDeContasRepositorio repoPalnoDeContas = new PlanoDeContasRepositorio();
       
        public ViewResult Index()
        {

            return View(repoPalnoDeContas.List(UsuarioSessao().Id));
        }

        public ViewResult Details(int id)
        {
            ViewBag.TipoPlano = new SelectList(new[] { new { Value=((int)TipoPlano.Despesas).ToString(), Text=TipoPlano.Despesas.ToString(), Selected=true },
                                                        new { Value=((int)TipoPlano.Receitas).ToString(), Text=TipoPlano.Receitas.ToString(), Selected=false } },
                                                "Value", "Text", "Selected");

            PlanoDeConta planodeconta = repoPalnoDeContas.Detalhes(id, UsuarioSessao().Id);
            return View(planodeconta);
        }

         public ActionResult Create()
        {
            ViewBag.TipoPlano = new SelectList(new[] { new { Value=((int)TipoPlano.Despesas).ToString(), Text=TipoPlano.Despesas.ToString(), Selected=true },
                                                        new { Value=((int)TipoPlano.Receitas).ToString(), Text=TipoPlano.Receitas.ToString(), Selected=false } },
                                                "Value", "Text", "Selected");

            return View();
        } 

     
        [HttpPost]
        public ActionResult Create(PlanoDeConta planodeconta)
        {
            if (ModelState.IsValid)
            {
                repoPalnoDeContas.Incluir(planodeconta,UsuarioSessao());
                
                return RedirectToAction("Index");  
            }

            return View(planodeconta);
        }
        
     
        public ActionResult Edit(int id)
        {
            ViewBag.TipoPlano = new SelectList(new[] { new { Value=((int)TipoPlano.Despesas).ToString(), Text=TipoPlano.Despesas.ToString(), Selected=true },
                                                        new { Value=((int)TipoPlano.Receitas).ToString(), Text=TipoPlano.Receitas.ToString(), Selected=false } },
                                                "Value", "Text", "Selected");

            PlanoDeConta planodeconta = repoPalnoDeContas.Detalhes(id, UsuarioSessao().Id);
            return View(planodeconta);
        }

       
        [HttpPost]
        public ActionResult Edit(PlanoDeConta planodeconta)
        {
            if (ModelState.IsValid)
            {
                repoPalnoDeContas.alterar(planodeconta.Id, planodeconta, UsuarioSessao().Id);
                return RedirectToAction("Index");
            }
            return View(planodeconta);
        }

      
 
        public ActionResult Delete(int id)
        {
            PlanoDeConta planodeconta = repoPalnoDeContas.Detalhes(id, UsuarioSessao().Id);
            return View(planodeconta);
        }

       

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            repoPalnoDeContas.Excluir(id, UsuarioSessao().Id); 
            return RedirectToAction("Index");
        }

    }
}