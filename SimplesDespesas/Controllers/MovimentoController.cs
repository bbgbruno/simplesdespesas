﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Modelos;

namespace SimplesDespesas.Controllers
{ 
    public class MovimentoController : Controller
    {
        private DataContext db = new DataContext();

        //
        // GET: /Movimento/

        public ViewResult Index()
        {
            return View(db.Movimentos.ToList());
        }

        //
        // GET: /Movimento/Details/5

        public ViewResult Details(int id)
        {
            Movimento movimento = db.Movimentos.Find(id);
            return View(movimento);
        }

        //
        // GET: /Movimento/Create

        public ActionResult Create()
        {
            return View();
        } 

        //
        // POST: /Movimento/Create

        [HttpPost]
        public ActionResult Create(Movimento movimento)
        {
            if (ModelState.IsValid)
            {
                db.Movimentos.Add(movimento);
                db.SaveChanges();
                return RedirectToAction("Index");  
            }

            return View(movimento);
        }
        
        //
        // GET: /Movimento/Edit/5
 
        public ActionResult Edit(int id)
        {
            Movimento movimento = db.Movimentos.Find(id);
            return View(movimento);
        }

        //
        // POST: /Movimento/Edit/5

        [HttpPost]
        public ActionResult Edit(Movimento movimento)
        {
            if (ModelState.IsValid)
            {
                db.Entry(movimento).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(movimento);
        }

        //
        // GET: /Movimento/Delete/5
 
        public ActionResult Delete(int id)
        {
            Movimento movimento = db.Movimentos.Find(id);
            return View(movimento);
        }

        //
        // POST: /Movimento/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {            
            Movimento movimento = db.Movimentos.Find(id);
            db.Movimentos.Remove(movimento);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}