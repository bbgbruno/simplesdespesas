﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Modelos;

namespace SimplesDespesas.Controllers
{
    public class AplicationController : Controller
    {
 
        public Usuario UsuarioSessao(){
            
           return  (Usuario)Session[" Usuario "];

        }

        public void SalvarUsuarioSessao(Usuario usuarioLogado)
        {
            Session[" Usuario "] = usuarioLogado;
        }
         
    }
}
