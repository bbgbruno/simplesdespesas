﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


namespace SimplesDespesas.Dominio.Modelos
{
    public class PlanoDeConta
    {
        public Guid Id { get; set; }
        public Guid UsuarioId { get; set; }
        public string Descricao { get; set; }
        public float Meta { get; set; }
        public virtual TipoMovimentoEnum nomeTipoMovimento { get { return (TipoMovimentoEnum)this.TipoMovimento; } }
        public bool NaoSomaNoResumo { get; set; }
        public TipoMovimentoEnum TipoMovimento { get; set; }
        
    }
}