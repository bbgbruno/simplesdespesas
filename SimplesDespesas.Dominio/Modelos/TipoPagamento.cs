﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SimplesDespesas.Dominio.Modelos
{
    
    public class TipoPagamento
    {
        public Guid Id { get; set; }
        public string Descricao { get; set; }
        public decimal limite { get; set; }
        public bool Avista { get; set; }
        public Guid UsuarioId { get; set; }
    
        public virtual Usuario Usuario { get; set; }

    }
}