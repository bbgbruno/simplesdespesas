﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SimplesDespesas.Dominio.Modelos
{
    public enum TipoMovimentoEnum : int
    {
        Receitas = 0,
        Despesas = 1
    }
 
    public class Movimento
    {
        public Guid Id { get; set; }
        public Guid PlanoDeContaID { get; set; }
        public Guid TipoPagamentoId { get; set; }
        public DateTime Data { get; set; }
        public DateTime Data_Evento { get; set; }
        public DateTime Vencimento { get; set; }
        public Decimal Valor { get; set; }
        public string Historico { get; set; }
        public bool Pago { get; set; }
        public int NumeroParcelas { get; set; }
        public virtual TipoMovimentoEnum nomeTipoMovimento { get { return (TipoMovimentoEnum)this.TipoMovimento; } }
        public TipoMovimentoEnum TipoMovimento  { get; set; }

        
        public virtual PlanoDeConta PlanoDeConta { get; set; }
        public virtual TipoPagamento TipoPagamento { get; set; }
    }
}
