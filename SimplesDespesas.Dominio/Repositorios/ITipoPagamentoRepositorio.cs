﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Modelos;

namespace Repositorio.Interfaces
{
    interface ITipoPagamentoRepositorio
    {

        List<TipoPagamento> List(int idUsuario);
        TipoPagamento Detalhes(int id, int idUsuario);
        void Incluir(TipoPagamento tipoPagamento, Usuario usuario);
        void alterar(int id, TipoPagamento novotipoPagamento, int idUsuario);
        void Excluir(int Id, int idUsuario);
    }
}
