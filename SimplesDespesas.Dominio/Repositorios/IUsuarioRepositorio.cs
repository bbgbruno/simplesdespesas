﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Modelos;

namespace Repositorio.Interfaces
{
    interface IUsuarioRepositorio
    {
        List<Usuario> List( );
        Usuario Detalhes(int id);
        void Incluir(Usuario usuario);
        void alterar(int id, Usuario novoUsuario);
        void Excluir(int Id);
    }
}
