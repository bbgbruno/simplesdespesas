﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Modelos;

namespace Repositorio.Interfaces
{
    interface IMovimentoRepositorio
    {
        List<Movimento> List(int idUsuario);
        Movimento Detalhes(int id, int idUsuario);
        void Incluir(Movimento movimento, Usuario usuario);
        void alterar(int id, Movimento novoMovimento, int idUsuario);
        void Excluir(int Id, int idUsuario);
    }
}
