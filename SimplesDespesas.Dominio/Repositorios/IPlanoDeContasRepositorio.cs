﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Modelos;

namespace Repositorio.Interfaces
{
    interface IPlanoDeContasRepositorio
    {
        List<PlanoDeConta> List(int idUsuario);
        PlanoDeConta Detalhes(int id, int idUsuario);
        void Incluir(PlanoDeConta planodeconta,Usuario usuario);
        void alterar(int id, PlanoDeConta novoPlanoDeConta, int idUsuario);
        void Excluir(int Id, int idUsuario);

    }
}
