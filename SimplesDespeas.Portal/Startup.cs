﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(SimplesDespeas.Portal.Startup))]
namespace SimplesDespeas.Portal
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
