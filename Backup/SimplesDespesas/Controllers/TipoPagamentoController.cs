﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Modelos;

namespace SimplesDespesas.Controllers
{ 
    public class TipoPagamentoController : Controller
    {
        private DataContext db = new DataContext();

        //
        // GET: /TipoPagamento/

        public ViewResult Index()
        {
            return View(db.TipoPagamentos.ToList());
        }

        //
        // GET: /TipoPagamento/Details/5

        public ViewResult Details(int id)
        {
            TipoPagamento tipopagamento = db.TipoPagamentos.Find(id);
            return View(tipopagamento);
        }

        //
        // GET: /TipoPagamento/Create

        public ActionResult Create()
        {
            ViewBag.TipoConta = new SelectList(new[] { new { Value=((int)TipoConta.Avista).ToString(), Text=TipoConta.Avista.ToString(), Selected=true },
                                                        new { Value=((int)TipoConta.Cartao).ToString(), Text=TipoConta.Cartao.ToString(), Selected=false } },
                                                "Value", "Text", "Selected");

            
            return View();
        } 

        //
        // POST: /TipoPagamento/Create

        [HttpPost]
        public ActionResult Create(TipoPagamento tipopagamento)
        {
            if (ModelState.IsValid)
            {
                db.TipoPagamentos.Add(tipopagamento);
                db.SaveChanges();
                return RedirectToAction("Index");  
            }

            return View(tipopagamento);
        }
        
        //
        // GET: /TipoPagamento/Edit/5
 
        public ActionResult Edit(int id)
        {
            TipoPagamento tipopagamento = db.TipoPagamentos.Find(id);
            return View(tipopagamento);
        }

        //
        // POST: /TipoPagamento/Edit/5

        [HttpPost]
        public ActionResult Edit(TipoPagamento tipopagamento)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tipopagamento).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(tipopagamento);
        }

        //
        // GET: /TipoPagamento/Delete/5
 
        public ActionResult Delete(int id)
        {
            TipoPagamento tipopagamento = db.TipoPagamentos.Find(id);
            return View(tipopagamento);
        }

        //
        // POST: /TipoPagamento/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {            
            TipoPagamento tipopagamento = db.TipoPagamentos.Find(id);
            db.TipoPagamentos.Remove(tipopagamento);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}